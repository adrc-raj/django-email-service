import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_email_engine.settings")
django.setup()
from django.http import HttpResponse
from django.db import connections, connection, transaction
import string
import json
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from dee.models import *
from dee.Email_File import Email_Class
from dee.config import EMAIL_UNAME, EMAIL_PASS, ADMIN_EMAIL
import logging
logger = logging.getLogger(__name__)

def email_report():
    response = {}
    try:
        response['email_sent'] = []
        start_time = (datetime.utcnow()) + relativedelta(minutes=300)
        end_time = (datetime.utcnow()) + relativedelta(minutes=330)

        time_report = EmailHistory.objects.filter(datetime__range = \
                      [start_time, end_time]).values('datetime','status')\
                      .order_by('-datetime')

        total_count = EmailHistory.objects.values('datetime').count()

        report_table = "<table><th><td>SN</td><td>DATE</td>\
                                        <td>STATUS</td></th>"
        sn = 0
        for eh in time_report:
            sn = sn+1
            report_table = report_table + "<tr><td>"+str(sn)+"</td>\
                                    <td>"+str(eh['datetime'])+"</td>\
                                    <td>"+str(eh['status'])+"</td></tr>"
        report_table = report_table +"</table>"
        response['email_sent'] = total_count

        mail_from = EMAIL_UNAME
        password = EMAIL_PASS
        #mail_to is Admin email id 
        mail_to = ADMIN_EMAIL

        subject = "Scheduled E-Mail Report"
        message = "Hi,<br/> This is the e-mail report below- <br/> \
                  Total E-Mail Sent: <b>"+str(total_count)+"</b><br/> E-Mail Sent in Last \
                  30 Minutes: <b>"+str(sn)+"</b> <br/> "+report_table

        email = Email_Class(mail_to,subject,message,'',\
                            mail_from, password,"",''\
                            , '')
        email.send_mail()

    except Exception as e:
        logger.error('Error in Email report to ADMIN '+str(e))

    return HttpResponse(json.dumps(response),\
                                     content_type='application/json')
email_report()