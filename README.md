**A. To Start With the django-email-service Project follow below steps-**
1. Create a virtual wrapper and environment.
2. Clone the django-email-service repo
3. Install Requirements.txt Using: pip install -r requirement.txt
4. Now create tables and connect with db using: python manage.py makemigrations, python manage.py migrate
5. To make a super-user use command:  python manage.py createsuperuser
6. Now Run the Project using: python manage.py runserver, You can see starting development server at http://127.0.0.1:8000/
7. To Configure the E-Mail Credentials, Edit the "config.py" under the Django app named /dee, Update the Uname, password, and admin E-Mail Id.
8. On Dashboard You can see the logs of sent mail with status, time, sub etc. You can change the column visiblity from the menu in header, You can do Sorting, search, export the email logs.
9. A Button "Compose an E-Mail" is Showing, To send a mail click on button it will open compose an email window.
10. Now You can send the Mail to multiple users by adding comma seprated e-mails in TO, CC, BCC
11. You Can Upload the TO, CC, BCC emails using CSV. (The CSV Should have columns in described order- TO, CC, BCC and values in rows accordingly)
12. On Send Button it check and verify the Required fields (TO, Sub) and the validate the email ID's entered in TO, CC, BCC using regular expression.
13. After Mail send it shows a Notification "Snack Bar" of message sent and clear the textboxes for new mail.
14. If you want to clear the values then you can click the clear button.
15. You can also Write HTML in email message and it will send the html rendred mail.
16. Code commenting is not done as not enough time spent but loging is done.

**B. To Send an email After every 30 mins to admin with all the email statistics of a day i.e; total email sent and timestamp of each email-**
1. There is a file task.py under project directory, Add file in the crontab.
2.Use crontab -e command to open crontab and append the command */30 * * * * /usr/bin/python /home/my/path/task.py
3. The script will send an email to admin email-id configured above in Point-7 including Total Email-Sent till now, Total email-sent in last 30 minutes. and list of email sent in last 30 minutes with timestemp.

**C. Use ELK stack for log management-**
##1. ELK Stack is comprised of three products i.e. Elasticsearch, Logstash, and Kibana. All of these are open source products by Elastic.
- Elasticsearch is a distributed RESTful search engine built for the cloud.
- Logstash is an open source, server-side data processing pipeline that receives data from a multitude of sources simultaneously transforms it, and then sends it to your target stash.
- Kibana is a browser-based analytics and search dashboard for Elasticsearch and is your window into the Elastic Stack.
##2. Set Up ELK with Docker
- Add GPG key for official Docker repository using : sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
- Add Docker repository to APT sources using: sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
- Update the package database with the Docker packages using: sudo apt-get update
- Check that installation source is the Docker repo: apt-cache policy docker-engine
- Install Docker using: sudo apt-get install -y docker-engine
- Check that it’s running : sudo systemctl status docker
- Docker by default requires root privileges, to avoid adding sudo to every docker command add the user to docker group: sudo usermod -aG docker
- Install Docker compose: udo curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)"
- set the permissions: sudo chmod +x /usr/local/bin/docker-compose
- Verify and check version using command : docker-compose -v
- Setup Docker Container for ELK using this GitHub repo docker-elk: git clone https://github.com/deviantony/docker-elk
- Jump into the docker-elk directory
- Launch the stack using command :  docker-compose up -d
- You can check the status: docker ps
##3. Use ELK in Django app
- Install- Install pip install python-logstash 
- In our settings.py file add code below-
LOGGING = {
    # ...
    'handlers': {
        # ...
        'logstash': {
            'level': 'INFO',
            'class': 'logstash.TCPLogstashHandler',
            'port': 5000,
            'host': '192.0.0.1', # IP/Name of the EC2 instance,
            'version': 1,
            'message_type': 'logstash',
            'fqdn': True,
            'tags': ['meetnotes'],
        },
    },
'logger': {
        # ...
        'app_name': {
            # file will accept only ERROR and higher level
            'handlers': ['file', 'logstash'],
            'level': DEBUG,
        },
    },
}

- Update file logstash.conf using : cd ~/docker-elk/logstash/pipeline/   and nano logstash.conf
-Update input and add codec information

 input {
       tcp {
                port => 5000
                codec => json
       }
 }


##4. Run Kibana on browser using url : http://0.0.0.0:5601
Here we can configure and integrate our app using "Add Data to Kibana">"Add Loging Data"



**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).