from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import string
import json
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from dee.models import *
from dee.Email_File import Email_Class
from dee.config import EMAIL_UNAME, EMAIL_PASS
import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'dee/index.html', {})
  
def view_email_history(request):
    response = {}
    try:
        response['projects'] = []
        ehistory = EmailHistory.objects.values().all().order_by('-id')
        for eh in ehistory:
            response['projects'].append([eh['mail_from'],eh['mail_to'],\
                                        eh['mail_cc'], eh['mail_bcc'],\
                                        eh['subject'], eh['message'],\
                                        eh['datetime'],eh['status']])
    except Exception as e:
        logger.error('Error in Get Mail History '+str(e))

    return HttpResponse(json.dumps(response, default = myconverter),\
                                     content_type='application/json')

@csrf_exempt
def view_compose_email(request):

    mail_from = request.POST.get("mail_from")
    mail_to = request.POST.get("mail_to")
    mail_cc = request.POST.get("mail_cc")
    mail_bcc = request.POST.get("mail_bcc")
    subject = request.POST.get("subject")
    message = request.POST.get("message")
    password = ''
    response = {}
    try:
        now = (datetime.utcnow()) + relativedelta(minutes=330)
        mail_from = EMAIL_UNAME
        password = EMAIL_PASS
        email = Email_Class(mail_to,subject,message,'',\
                            mail_from, password,"",mail_cc\
                            , mail_bcc)
        
        status = email.send_mail()
        EmailHistory.objects.create(\
                                mail_from=mail_from,\
                                mail_to=mail_to,\
                                mail_cc=mail_cc,\
                                mail_bcc=mail_bcc,\
                                subject=subject,\
                                message=message,\
                                datetime=now,\
                                status=status)
        response['status'] = status      
    except Exception as e:
        response['status'] = 0
        logger.error('Error in Mail Compose '+str(e))
    return HttpResponse(json.dumps(response, default = myconverter),\
                                     content_type='application/json')

def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()
