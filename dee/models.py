from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class EmailHistory(models.Model):
  """Stores information of products.
  """
  id = models.AutoField(primary_key=True)
  mail_from = models.CharField(max_length=50, null=True)
  mail_to = models.CharField(max_length=500, null=True)
  mail_cc = models.CharField(max_length=500, null=True)
  mail_bcc = models.CharField(max_length=500, null=True)
  subject = models.CharField(max_length=100, null=True)
  message = models.TextField()
  datetime = models.DateTimeField()
  status = models.BooleanField()
  class Meta:
    unique_together = (("id"), )