from django.conf.urls import patterns, url

from dee import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^compose_email$', views.view_compose_email, name='view_compose_email'),
    url(r'^email_history$', views.view_email_history, name='view_email_history'),
)