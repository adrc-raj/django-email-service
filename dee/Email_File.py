from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import formatdate
from email import Encoders
import traceback
import os
import smtplib,sys,os
import logging
logger = logging.getLogger(__name__)

class Email_Class:
	toaddrs=None
	sub=None
	message=None
	file_list=None
	cc_list=None
	bcc_list=None
	user_name=None
	password=None
	path=""
	def __init__(self,toaddrs,sub,message,file_list,user_name,password,path="",cc_list=None,bcc_list=None):
		self.toaddrs=toaddrs
		self.message=message
		self.sub=sub
		self.file_list=file_list
		self.cc_list=cc_list
		self.user_name=user_name
		self.password=password
		self.path=path

	def send_mail(self):
		print 'uname', self.user_name
		print 'cc', self.cc_list
		try:
			toaddrs=self.toaddrs
			addr_list=[]
			#addr_list=[toaddrs,]
			for email_id in (self.toaddrs).split(','):
				addr_list.append(email_id)
			fromaddr = self.user_name
			msg = MIMEMultipart()
			msg['From'] = self.user_name
			msg['To'] = self.toaddrs
			msg['Cc'] = self.cc_list
			msg['Bcc'] = self.bcc_list
			# for email_id in self.cc_list:
			# 	addr_list.append(email_id)
			try:
				for email_id in (self.cc_list).split(','):
					addr_list.append(email_id)
			except:
				pass
			try:
				for email_id in (self.bcc_list).split(','):
					addr_list.append(email_id)
			except:
				pass
			msg['Subject'] = self.sub
			#msg.attach( MIMEText(self.message, , 'plain') )
			#part1 = MIMEText(self.message, 'plain')
			part2 = MIMEText(self.message, 'html')
			#msg.attach( part1 )
			msg.attach( part2 )
			part = MIMEBase('application', "octet-stream")
			for f in self.file_list:
				part = MIMEBase('application', "octet-stream")
				part.set_payload( open(self.path+f,"rb").read() )
				Encoders.encode_base64(part)
				part.add_header('Content-Disposition', 'attachment; filename="{0}"'.\
					format(os.path.basename(f)))
				msg.attach(part)
			username = self.user_name
			password = self.password
			server = smtplib.SMTP('smtp.gmail.com',587)
			server.starttls()
			server.login(username,password)
			server.sendmail(fromaddr, addr_list, msg.as_string())
			server.quit()
			return True
		except Exception as e:
			logger.error('Error in Mail Send '+str(e))
			#traceback.print_exc()
			return False
